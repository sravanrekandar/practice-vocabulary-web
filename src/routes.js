import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';
import HomePage from './components/HomePage';
import AboutPage from './components/AboutPage';
import AllWordsPageContainer from './modules/allWords/AllWordsPageContainer';
import WordPageContainer from './modules/word/WordPageContainer';
import NotFoundPage from './components/NotFoundPage';

export default (
  <Route path="/" component={App}>
    {/*
    <IndexRoute component={HomePage}/>
    */}
    <IndexRoute component={AllWordsPageContainer}/>
    <Route path="home" component={HomePage}/>
    <Route path="about" component={AboutPage}/>
    <Route path="all-words" component={AllWordsPageContainer}/>
    <Route path="new-word" component={WordPageContainer}/>
    <Route path="manage-word" component={WordPageContainer}/>
    <Route path="*" component={NotFoundPage}/>
  </Route>
);

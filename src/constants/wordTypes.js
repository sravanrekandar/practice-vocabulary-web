const WORD_TYPES = [
  "VERB",
  "ADJECTIVE",
  "NOUN",
  "PHRASEL VERB",
  "PREPOSITION",
  "ADVERB",
  "CONJUNCTION",
  "PRONOUN"
];
export default WORD_TYPES;

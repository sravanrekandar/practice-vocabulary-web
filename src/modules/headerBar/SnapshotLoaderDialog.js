import React, { PropTypes } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

export default class LanguagesDialog extends React.Component {
    static propTypes = {
        onClose: PropTypes.func
    }

    render() {
        const actions = [
            <FlatButton
                key="lang-dialog-button-cancel"
                label="Cancel"
                primary
                onTouchTap={this.props.onClose}
            />,
            <FlatButton
                key="lang-dialog-button-save"
                label="Save"
                primary
                onTouchTap={this.saveSnapshot.bind(this)}
            />,
        ];

        return (
            <div>
                <Dialog
                    title="All Translations"
                    actions={actions}
                    open
                >
                    <textarea ref={node => this.area = node} rows="15" cols="70">
                    </textarea>
                </Dialog>
            </div>
        );
    }
    saveSnapshot() {
        const x = {
            words: [],
            trash: [],
        };
        x.words = JSON.parse(this.area.value)
        localStorage.setItem('VOCAB_APP', JSON.stringify(x));
        alert('Snapshot saved successfully');
        this.props.onClose();
    }
}

import React, { Component, PropTypes } from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import { browserHistory } from 'react-router';
import FlatButton from 'material-ui/FlatButton';
import Palette from 'google-material-color-palette-json';
import SnapshotLoaderDialog from './SnapshotLoaderDialog';
import {
  resetWord
} from '../word/wordState';

const menuButtonStyle = {
  verticalAlign: 'top',
  marginTop: 5,
  color: Palette.white
};

export default class HeaderBar extends Component {
  static propTypes = {
    dispatch: PropTypes.func
  }
  constructor(props) {
    super(props);
    this.renderNavigationButtons = this.renderNavigationButtons.bind(this);
    this.renderLogged = this.renderLogged.bind(this);
    this.gotoRoute = this.gotoRoute.bind(this);
    this.loadFromSnapshot = this.loadFromSnapshot.bind(this);
    this.closeSnapshotLoader = this.closeSnapshotLoader.bind(this);
  }
  handleChange = (event, logged) => {
    this.setState({ logged: logged });
  };
  gotoRoute(route) {
    if (route === '/new-word') {
      this.props.dispatch(resetWord());
    }
    browserHistory.push(route);
  }
  renderNavigationButtons() {
    return (
      <IconButton>
        <NavigationClose />
      </IconButton>
    );
  }
  renderLogged(props) {
    const { gotoRoute } = this;
    return (
      <div>
        <FlatButton
          label="All words"
          style={menuButtonStyle}
          onClick={gotoRoute.bind(this, '/all-words')}
        />
        <FlatButton
          label="New Word"
          style={menuButtonStyle}
          onClick={gotoRoute.bind(this, '/new-word')}
        />
        <IconMenu
          {...props}
          iconButtonElement={
            <IconButton><MoreVertIcon /></IconButton>
          }
          targetOrigin={{ horizontal: 'right', vertical: 'top' }}
          anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
        >
          <MenuItem primaryText="All Words" onClick={this.gotoRoute.bind(this, '/all-words')} />
          <MenuItem primaryText="New Word" onClick={this.gotoRoute.bind(this, '/new-word')} />
          <MenuItem primaryText="Load From Snapshot" onClick={this.loadFromSnapshot} />
          <MenuItem primaryText="Sign out" />
        </IconMenu>
      </div>
    );
  }
  render() {
    return (
      <div>
        <AppBar
          title={(<span style={{ cursor: 'pointer' }}>Vocabulary Builder</span>)}
          iconElementLeft={this.renderNavigationButtons()}
          iconElementRight={this.renderLogged()}
          onTitleTouchTap={this.gotoRoute.bind(this, '/')}
        />
        {
          this.snapshotLoaderEnabled && (
            <SnapshotLoaderDialog onClose={this.closeSnapshotLoader} />
          )
        }
      </div>
    );
  }
  loadFromSnapshot() {
    this.snapshotLoaderEnabled = true;
    this.forceUpdate();
  }
  closeSnapshotLoader() {
    this.snapshotLoaderEnabled = false;
    this.forceUpdate();
  }
}

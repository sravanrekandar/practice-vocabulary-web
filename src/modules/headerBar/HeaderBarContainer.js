import {connect} from 'react-redux';
import HeaderBar from './HeaderBar';
function mapStateToProps() {
  return {

  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderBar);

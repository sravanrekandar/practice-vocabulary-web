import {connect} from 'react-redux';
import AllWordsPage from './AllWordsPage';
function mapStateToProps(state) {
  return {
    words: state.wordState.get('words'),
    trash: state.wordState.get('trash'),
    allTags: state.wordState.get('allTags')
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AllWordsPage);

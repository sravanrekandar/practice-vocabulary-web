import React, { PropTypes, Component } from 'react';
import { List } from 'immutable';
import FlatButton from 'material-ui/FlatButton';
import { browserHistory } from 'react-router';
import Palette from 'google-material-color-palette-json';
import _ from 'lodash';
import WordView from '../word/WordView';
import Tags from '../../components/Tags';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import DeleteIcon from 'material-ui/svg-icons/action/delete-forever';
import RestoreIcon from 'material-ui/svg-icons/action/assignment-return';
import DownloadIcon from 'material-ui/svg-icons/file/file-download';
import download from 'in-browser-download';

import {
  getWords,
  getTrash,
  receiveWord,
  deleteWord,
  deleteWordPermenantly,
  restoreWord,
  getTags,
} from '../word/wordState';

export default class AllWords extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    words: PropTypes.instanceOf(List),
    trash: PropTypes.instanceOf(List),
    allTags: PropTypes.instanceOf(List),
    filterTags: PropTypes.instanceOf(List),
  }
  constructor(props) {
    super(props);
    this.onEditWord = this.onEditWord.bind(this);
    this.onDownload = this.onDownload.bind(this);
    this.onRemoveTag = this.onRemoveTag.bind(this);
    this.onAddTag = this.onAddTag.bind(this);
    this.onDownloadFilteredWords = this.onDownloadFilteredWords.bind(this);

    this.state = {
      filterTags: []
    };
  }
  componentWillMount() {
    this.props.dispatch(getWords());
    this.props.dispatch(getTrash());
    this.props.dispatch(getTags());
  }
  onEditWord(word) {
    const {dispatch} = this.props;
    dispatch(receiveWord(word));
    browserHistory.push('/manage-word');
  }
  onDeleteWord(word) {
    this.props.dispatch(deleteWord(word));
  }
  onDeleteWordPermanantly(word) {
    this.props.dispatch(deleteWordPermenantly(word));
  }
  onRestoreWord(word) {
    this.props.dispatch(restoreWord(word));
  }
  onDownload() {
    download(JSON.stringify(this.props.words.toJSON()), `words-${Date.now()}.json`);
  }
  onDownloadFilteredWords() {
    const words = this.props.words.toJS();
    const filterTags = this.state.filterTags;
    let filteredWords = words;
    if (filterTags.length > 0) {
      filteredWords = this.filterWordsByTags(words, filterTags);
    }
    download(JSON.stringify(filteredWords), `filtered-words-${Date.now()}.json`);
  }
  onRemoveTag(tagName) {
    const {filterTags} = this.state;
    const tagIndex = filterTags.indexOf(tagName);
    filterTags.splice(tagIndex, 1);
    this.setState({
      filterTags
    });
  }
  onAddTag(tagName) {
    const {filterTags} = this.state;
    const tagIndex = this.props.allTags.toJS().indexOf(tagName);
    if (tagIndex !== -1) {
      filterTags.push(tagName);
      this.setState({
        filterTags
      });
    }

  }
  filterWordsByTags(words, filterTags){
    return words.filter(e => {
      return _.intersection(e.tags, filterTags).length > 0;
    });
  }
  render() {
    const words = this.props.words.toJS();
    const trash = this.props.trash.toJS();
    const filterTags = this.state.filterTags;
    let allTags = this.props.allTags.toJS();
    allTags = allTags.filter(e => filterTags.indexOf(e) === -1);

    let filteredWords = words;
    if (filterTags.length > 0) {
      filteredWords = this.filterWordsByTags(words, filterTags);
    }

    return (
      <div>
        <h1>
          All Words
          {' '}
          <FlatButton
            labelPosition="before"
            primary={true}
            icon={<DownloadIcon />}
            onClick={this.onDownload}
          />
          <Tags
            setId="wordsFilterTags"
            label="words Filter"
            hintText="filter tag"
            allTags={allTags}
            tags={this.state.filterTags}
            onRemoveTag={this.onRemoveTag}
            onAddTag={this.onAddTag}
          />
        </h1>
        <p className="text-muted">
          showing {filteredWords.length} of {words.length} words
          {
            (words.length === filteredWords.length)
            ? <span />
            : (
              <FlatButton
                label={`download filtered words`}
                labelPosition="before"
                primary={true}
                icon={<DownloadIcon />}
                onClick={this.onDownloadFilteredWords}
              />
            )
          }
        </p>
        <ul className="list-unstyled">
          {
            filteredWords.map((word, idx) => (
              <li key={`word-view-${idx}`} style={{ paddingLeft: 5, backgroundColor: (idx % 2 === 1) ? Palette.grey.shade_100 : '' }}>
                <hr />
                <div className="row">
                  <div className="col-xs-12 col-md-6">
                    <WordView word={word} />
                  </div>
                  <div className="col-xs-12 col-md-6">
                    <FlatButton
                      label={`edit "${word.wordText}"`}
                      labelPosition="before"
                      primary={true}
                      icon={<EditIcon />}
                      onClick={this.onEditWord.bind(this, word)}
                    />
                    <br />
                    <FlatButton
                      label={`delete`}
                      labelPosition="before"
                      secondary={true}
                      icon={<DeleteIcon />}
                      onClick={this.onDeleteWord.bind(this, word)}
                    />
                  </div>
                </div>

              </li>
            ))
          }
        </ul>

        <hr />
        <ul className="list-unstyled">
          {
            trash.map((word, idx) => (
              <li key={`word-view-${idx}`} style={{ backgroundColor: Palette.red.shade_50, paddingLeft: 5, paddingRight: 5 }}>
                <hr />
                <div>
                  <FlatButton
                    label={`Restore`}
                    labelPosition="before"
                    primary={true}
                    icon={<RestoreIcon />}
                    onClick={this.onRestoreWord.bind(this, word)}
                  />
                  <FlatButton
                    label={`delete - "${word.wordText}" forever`}
                    labelPosition="before"
                    secondary={true}
                    icon={<DeleteIcon />}
                    onClick={this.onDeleteWordPermanantly.bind(this, word)}
                  />
                </div>
                <WordView word={word} />
              </li>
            ))
          }
        </ul>
      </div>
    );
  }
}

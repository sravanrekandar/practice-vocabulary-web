import _ from 'lodash';
const VOCAB_APP = 'VOCAB_APP';
let appData = {
  words: [],
  trash: []
};
export function getWords(){
  return _.cloneDeep(appData.words);
}
export function getTrash(){
  return _.cloneDeep(appData.trash);
}

export function saveWord(word){
  const {words} = appData;
  const existingWordIndex = words.findIndex((e) => e.wordText.toLowerCase() === word.wordText.toLowerCase());
  if(existingWordIndex === -1) {
    words.push(word);
  } else {
    words.splice(existingWordIndex, 1, word);
  }
  save();
}
export function deleteWord(word) {
  const {words, trash} = appData;
  const existingWordIndex = words.findIndex((e) => e.wordText.toLowerCase() === word.wordText.toLowerCase());
  if(existingWordIndex !== -1) {
    words.splice(existingWordIndex, 1);
    trash.push(word);
  }
  save();
}
export function deleteWordPermenantly(word) {
  const {trash} = appData;
  const existingWordIndex = trash.findIndex((e) => e.wordText.toLowerCase() === word.wordText.toLowerCase());
  if(existingWordIndex !== -1) {
    trash.splice(existingWordIndex, 1);
  }
  save();
}
export function restoreWord(word){
  const {words, trash} = appData;
  const existingWordIndex = trash.findIndex((e) => e.wordText.toLowerCase() === word.wordText.toLowerCase());
  if(existingWordIndex !== -1) {
    trash.splice(existingWordIndex, 1);
    words.push(word);
  }
  save();
}
export function getTags(){
  let extractions = appData.words.reduce((a, b) => ({ tags: [].concat(a.tags, b.tags) }), {tags:[]});
  return _.uniq(_.cloneDeep(extractions.tags));
}
export function save(){
  localStorage.setItem(VOCAB_APP, JSON.stringify(appData));
}
export function init(){
  let store = localStorage.getItem(VOCAB_APP);
  if(!store){
    save();
  } else {
    appData = Object.assign({}, appData, JSON.parse(store));
    appData.words.forEach(w => {
      w.definitions.forEach(d => {
        d.otherLanguages  = d.otherLanguages || d.otherLangauges;
        delete d.otherLangauges;
      });
    });
  }
}
init();

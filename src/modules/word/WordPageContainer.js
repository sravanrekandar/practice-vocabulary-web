import {connect} from 'react-redux';
import WordPage from './WordPage';
function mapStateToProps(state) {
  return {
    word: state.wordState.get('word'),
    allTags: state.wordState.get('allTags'),
    routes: state.routes
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WordPage);

import React, { PropTypes, Component } from 'react';
import { Card, CardText } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import ViewListIcon from 'material-ui/svg-icons/action/view-list';
import Binder from 'react-binding';

import LanguagesDialog from './LanguagesDialog';
import WORD_TYPES from '../../constants/wordTypes';
import TextBoxInput from '../../components/TextBoxInput';
import SelectFieldBox from '../../components/SelectFiledBox';
import Tags from '../../components/Tags';

export default class DefinitionCard extends Component {
  static propTypes = {
    definitionId: PropTypes.string,
    title: PropTypes.string,
    onAddSynonym: PropTypes.func.isRequired,
    onRemoveSynonym: PropTypes.func.isRequired,
    model: PropTypes.object,
  }
  constructor(props) {
    super(props);
    this.onLanguagesDialogOpen = this.onLanguagesDialogOpen.bind(this);
    this.onLanguagesDialogClose = this.onLanguagesDialogClose.bind(this);
  }
  onLanguagesDialogOpen() {
    this.languagesDialogEnabled = true;
    console.log('open')
    this.forceUpdate();
  }
  onLanguagesDialogClose() {
    this.languagesDialogEnabled = false;
    this.forceUpdate();
  }
  render() {
    return (
      <div>
        <Card>
          <CardText>
            <div className="clearfix">
              <div className="pull-left">
                <SelectFieldBox
                  items={WORD_TYPES}
                  model={Binder.bindTo(this.props.model, "type")}
                  title="type"
                />
              </div>
              <div className="pull-right">
                <strong>{this.props.title}</strong>
              </div>
            </div>
            <div>
              <TextBoxInput
                floatingLabelText="Definition"
                hintText="Definition...."
                model={Binder.bindTo(this.props.model, "definitionText")}
                fullWidth={true}
              />
              {
                this.props.model.value.examples.map((ex, idx) => (
                  <TextBoxInput
                    key={`${this.props.definitionId}-example-${idx}`}
                    floatingLabelText={`Example ${idx + 1}`}
                    hintText={`Example ${idx + 1}`}
                    fullWidth={true}
                    model={Binder.bindTo(this.props.model, `examples[${idx}]`)}
                  />
                ))
              }
            </div>
          </CardText>
          <CardText>
            <div className="row">
              <div className="col-xs-6 text-center">
                <TextBoxInput
                  floatingLabelText="తెలుగు (telugu)"
                  hintText="తెలుగు పదం"
                  model={Binder.bindTo(this.props.model, "otherLanguages.telugu")}
                  fullWidth={true}
                />
              </div>
              <div className="col-xs-6 text-center">
                <TextBoxInput
                  floatingLabelText="हिंदी (hindi)"
                  hintText="हिंदी शब्द"
                  model={Binder.bindTo(this.props.model, "otherLanguages.hindi")}
                  fullWidth={true}
                />
              </div>
              <div className="col-xs-6 text-center">
                <TextBoxInput
                  floatingLabelText="தமிழ் (tamil)"
                  hintText="தமிழ் வார்த்தை"
                  model={Binder.bindTo(this.props.model, "otherLanguages.tamil")}
                  fullWidth={true}
                />
              </div>
              <div className="col-xs-6 text-center">
                <FlatButton
                  label="...more langs"
                  labelPosition="before"
                  icon={<ViewListIcon />}
                  style={{ marginTop: '5px' }}
                  onClick={this.onLanguagesDialogOpen}
                />
              </div>
            </div>
            <div>
              <Tags
                hintText="enter synonym"
                label="Synonyms"
                onAddTag={this.props.onAddSynonym}
                onRemoveTag={this.props.onRemoveSynonym}
                tags={this.props.model.value.synonyms}
              />
            </div>
          </CardText>
        </Card >

        {
          this.languagesDialogEnabled && (
            <LanguagesDialog
              onLanguagesDialogOpen={this.onLanguagesDialogOpen}
              open
              onLanguagesDialogClose={this.onLanguagesDialogClose}
              model={this.props.model}
            />
          )
        }

      </div>
    );
  }
}

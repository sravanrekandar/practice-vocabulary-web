import React, { PropTypes, Component } from 'react';
import { Map, List } from 'immutable';
import RaisedButton from 'material-ui/RaisedButton';
import ViewListIcon from 'material-ui/svg-icons/content/save';
import Binder from 'react-binding';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';

import TextBoxInput from '../../components/TextBoxInput';
import DefinitionCard from './DefinitionCard';
import Tags from '../../components/Tags';
import WordView from './WordView';

import {
  saveWord,
  getTags,
} from './wordState';


export default class WordPage extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    word: PropTypes.instanceOf(Map),
    allTags: PropTypes.instanceOf(List)
  }

  constructor(props) {
    super(props);
    this.onRemoveTag = this.onRemoveTag.bind(this);
    this.onAddTag = this.onAddTag.bind(this);
    this.onAddSynonym = this.onAddSynonym.bind(this);
    this.onRemoveSynonym = this.onRemoveSynonym.bind(this);
    this.openWordViewDialog = this.openWordViewDialog.bind(this);
    this.closeWordViewDialog = this.closeWordViewDialog.bind(this);
    this.saveWord = this.saveWord.bind(this);
  }

  state = {
    languagesDialogOpen: false,
    wordViewDialogEnabled: false,
  }
  componentWillMount() {
    this.props.dispatch(getTags());
    this.setState({
      word: this.props.word.toJS()
    });
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      word: nextProps.word.toJS()
    });
  }
  onLanguagesDialogOpen = () => {
    this.setState({ languagesDialogOpen: true });
  }

  onLanguagesDialogClose = () => {
    this.setState({ languagesDialogOpen: false });
  }
  onRemoveTag(tagName) {
    const tag = tagName.toLowerCase();
    const { word } = this.state;
    const tagIndex = word.tags.indexOf(tag);

    if (tagIndex !== -1) {
      word.tags.splice(tagIndex, 1);
      this.setState({
        word: word
      });
    }
  }
  onAddTag(tagName) {
    const newTag = tagName.toLowerCase();
    const { word } = this.state;
    const tagIndex = word.tags.indexOf(newTag.toLowerCase());

    if (tagIndex === -1) {
      word.tags.push(newTag);
      this.setState({
        word: word
      });
    }
  }
  onRemoveSynonym(definitionIndex, synonym) {
    const { word } = this.state;
    const definition = word.definitions[definitionIndex];
    const synonymIndex = definition.synonyms.indexOf(synonym.toLowerCase());
    if (synonymIndex !== -1) {
      definition.synonyms.splice(synonymIndex, 1);
      this.setState({
        word: word
      });
    }
  }
  onAddSynonym(definitionIndex, synonym) {
    const newSynonym = synonym.toLowerCase();
    const { word } = this.state;
    const definition = word.definitions[definitionIndex];
    const synonymIndex = definition.synonyms.indexOf(newSynonym);
    if (synonymIndex === -1) {
      definition.synonyms.push(newSynonym);
      definition.synonyms.sort();
      this.setState({
        word: word
      });
    }
  }
  saveWord() {
    this.closeWordViewDialog();
    this.props.dispatch(saveWord(this.state.word));
  }
  openWordViewDialog() {
    if (this.state.word.wordText) {
      this.setState({
        wordViewDialogEnabled: true
      });
    }
  }
  closeWordViewDialog() {
    this.setState({
      wordViewDialogEnabled: false
    });
  }
  render() {
    return (
      <div >
        <div className="text-center">
          <RaisedButton
            label="Save Word"
            labelPosition="before"
            primary={true}
            icon={<ViewListIcon />}
            style={{ marginTop: '5px' }}
            onClick={this.openWordViewDialog}
          />
        </div>
        <div>
          <TextBoxInput
            hintText="friendship"
            floatingLabelText="Word text"
            model={Binder.bindToState(this, "word", "wordText")}
          />
          <TextBoxInput
            hintText="/ˈfrɛn(d)ʃɪp/"
            floatingLabelText="Phonetic script"
            model={Binder.bindToState(this, "word", "phoneticScript")}
          />
          <TextBoxInput
            hintText="frend-ship"
            floatingLabelText="Syllable"
            model={Binder.bindToState(this, "word", "syllable")}
          />
          <Tags
            hintText="new tag"
            label="Tags"
            tags={this.state.word.tags}
            setId="tags"
            allTags={this.props.allTags.toJS()}
            onRemoveTag={this.onRemoveTag}
            onAddTag={this.onAddTag}
          />
        </div>
        <div className="row">
          {
            this.state.word.definitions.map((def, idx) => (
              <div className="col-md-6" key={`definition-${idx}`}>
                <DefinitionCard
                  title={`Definition ${idx + 1}`}
                  definitionId="definition1"
                  model={Binder.bindToState(this, "word", `definitions[${idx}]`)}
                  onLanguagesDialogOpen={this.onLanguagesDialogOpen}
                  onAddSynonym={this.onAddSynonym.bind(this, idx)}
                  onRemoveSynonym={this.onRemoveSynonym.bind(this, idx)}
                />
              </div>
            ))
          }

        </div>
        <div className="text-center">
          <RaisedButton
            label="Save Word"
            labelPosition="before"
            primary={true}
            icon={<ViewListIcon />}
            style={{ marginTop: '5px' }}
            onClick={this.openWordViewDialog}
          />
        </div>
        <div>
          <Dialog
            title="Review Word"
            actions={[<FlatButton
              key="lang-dialog-button-cancel"
              label="Cancel"
              primary={true}
              onTouchTap={this.closeWordViewDialog}
            />, <FlatButton
              key="lang-dialog-button-save"
              label="Save"
              primary={true}
              onTouchTap={this.saveWord}
            />]}
            modal={true}
            open={this.state.wordViewDialogEnabled}
          >
            <WordView word={this.state.word} />
          </Dialog>

        </div>
      </div >
    );
  }
}


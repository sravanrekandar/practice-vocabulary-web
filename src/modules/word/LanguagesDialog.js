import React, { PropTypes } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Binder from 'react-binding';
import TextBoxInput from '../../components/TextBoxInput';

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class LanguagesDialog extends React.Component {
  static propTypes = {
    open: PropTypes.bool,
    onLanguagesDialogClose: PropTypes.func
  }

  render() {
    const actions = [
      <FlatButton
        key="lang-dialog-button-cancel"
        label="Save and Close"
        primary={true}
        onTouchTap={this.props.onLanguagesDialogClose}
      />
    ];

    return (
      <div>
        <Dialog
          title="All Translations"
          actions={actions}
          open={this.props.open}
        >
          <div className="row">
            <div className="col-xs-6 text-center">
              <TextBoxInput
                floatingLabelText="తెలుగు (telugu)"
                hintText="తెలుగు పదం"
                model={Binder.bindTo(this.props.model, "otherLanguages.telugu")}
                fullWidth={true}
              />
            </div>
            <div className="col-xs-6 text-center">
              <TextBoxInput
                floatingLabelText="हिंदी (hindi)"
                hintText="हिंदी शब्द"
                model={Binder.bindTo(this.props.model, "otherLanguages.hindi")}
                fullWidth={true}
              />
            </div>
            <div className="col-xs-6 text-center">
              <TextBoxInput
                floatingLabelText="தமிழ் (tamil)"
                hintText="தமிழ் வார்த்தை"
                model={Binder.bindTo(this.props.model, "otherLanguages.tamil")}
                fullWidth={true}
              />
            </div>
            <div className="col-xs-6 text-center">
              <TextBoxInput
                floatingLabelText="മലയാളം (malayalam)"
                hintText="മലയാളം വാക്കാണ്"
                model={Binder.bindTo(this.props.model, "otherLanguages.malayalam")}
                fullWidth={true}
              />
            </div>
            <div className="col-xs-6 text-center">
              <TextBoxInput
                floatingLabelText="ಕನ್ನಡ (kannada)"
                hintText="ಕನ್ನಡ ಭಾಷೆ"
                model={Binder.bindTo(this.props.model, "otherLanguages.kannada")}
                fullWidth={true}
              />
            </div>
            <div className="col-xs-6 text-center">
              <TextBoxInput
                floatingLabelText="मराठी (marathi)"
                hintText="मराठी शब्द"
                model={Binder.bindTo(this.props.model, "otherLanguages.marathi")}
                fullWidth={true}
              />
            </div>
            <div className="col-xs-6 text-center">
              <TextBoxInput
                floatingLabelText="मराठी (marathi)"
                hintText="मराठी शब्द"
                model={Binder.bindTo(this.props.model, "otherLanguages.marathi")}
                fullWidth={true}
              />
            </div>
            <div className="col-xs-6 text-center">
              <TextBoxInput
                floatingLabelText="বাংলা (bangla)"
                hintText="বাংলা শব্দ"
                model={Binder.bindTo(this.props.model, "otherLanguages.bangla")}
                fullWidth={true}
              />
            </div>
            <div className="col-xs-6 text-center">
              <TextBoxInput
                floatingLabelText="ਪੰਜਾਬੀ (punjabi)"
                hintText="ਪੰਜਾਬੀ ਸ਼ਬਦ"
                model={Binder.bindTo(this.props.model, "otherLanguages.punjabi")}
                fullWidth={true}
              />
            </div>
            <div className="col-xs-6 text-center">
              <TextBoxInput
                floatingLabelText="اردو (urdu)"
                hintText="لفظ ਪاردو"
                model={Binder.bindTo(this.props.model, "otherLanguages.urdu")}
                fullWidth={true}
              />
            </div>
          </div>
        </Dialog>
      </div>
    );
  }
}

import React, { PropTypes, Component } from 'react';
import Palette from 'google-material-color-palette-json';
import _ from 'lodash';
export function cleanWord(word) {
  word = _.cloneDeep(word);
  word.definitions = word.definitions.filter(d => !!d.definitionText);
  word.definitions.forEach(def => {
    def.examples = def.examples.filter(ex => !!ex);
    def.synonyms = def.synonyms.filter(sy => !!sy);
  });
  return word;
}
export default class WordView extends Component {
  renderDefinition(def, idx) {
    return (
      <div>
        <strong>{def.type.toLowerCase()}.</strong> {def.definitionText}
        <ul className="">
          {
            def.examples.map((ex, eIdx) => (
              <li
                key={`word-view-definition-${def.wordText}-${idx}-ex-${eIdx}`}
                className="text-muted"
              >
                {
                  (!ex)
                    ? <span />
                    : <span><em>Eg: </em> {ex}</span>
                }
              </li>
            ))
          }
          <li>
            <ul className="list-inline">
              {
                Object.keys(def.otherLanguages).map((lang) => (
                  <li key={`${def.definitionText}-lang-${lang}`}>
                    <muted>{lang}: </muted> <span>{def.otherLanguages[lang]}</span>
                  </li>
                ))
              }
            </ul>
          </li>
          <li>
            <em>Synonyms: </em>
            <span style={{ color: Palette.blueGrey.shade_400 }}>{def.synonyms.join(', ')}</span>
          </li>
        </ul>
      </div>
    );
  }
  render() {
    let {word} = this.props;
    word = cleanWord(word);
    return (
      <div>
        <div>
          <h2 className="text-inline">{word.wordText}</h2>
          {' '}
          <p className="text-muted text-inline">
            <span style={{ color: Palette.blue.shade_300 }}>
              {word.syllable}
            </span>
            {' '}
            <span style={{ color: Palette.deepPurple.shade_200 }}>{word.phoneticScript}</span>
          </p>
        </div>
        <ul className="list-unstyled">
          {
            word.definitions.map((def, idx) => (
              <li key={`word-view-definition-${def.wordText}-${idx}`}>
                {
                  (!def.definitionText)
                    ? <span />
                    : this.renderDefinition(def, idx)
                }
              </li>
            ))
          }
        </ul>
        <div>
          <strong><em>Tags: </em></strong>
          <span style={{ color: Palette.grey.shade_400 }}>{word.tags.join(', ')}</span>
        </div>
      </div>
    );
  }

}
WordView.propTypes = {
  word: PropTypes.object
};

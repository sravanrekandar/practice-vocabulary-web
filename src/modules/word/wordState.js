import { fromJS } from 'immutable';
import _ from 'lodash';
import newWordtemplate from './newWordTemplate';
import * as storageUtils from '../storageUtils';

export const RECEIVE_WORDS = 'RECEIVE_WORDS';
export const RECEIVE_TRASH = 'RECEIVE_TRASH';
export const RECEIVE_WORD = 'RECEIVE_WORD';
export const RECEIVE_TAGS = 'RECEIVE_TAGS';
export const RESET_WORD = 'RESET_WORD';

export function resetWord() {
  return {
    type: RESET_WORD
  };
}
export function receiveWord(word) {
  return {
    type: RECEIVE_WORD,
    word
  };
}
export function getTags() {
  return {
    type: RECEIVE_TAGS,
    allTags: storageUtils.getTags()
  };
}
export function getWords() {
  return (dispatch) => {
    dispatch({
      type: RECEIVE_WORDS,
      words: storageUtils.getWords()
    });
  };
}
export function getTrash() {
  return (dispatch) => {
    dispatch({
      type: RECEIVE_TRASH,
      trash: storageUtils.getTrash()
    });
  };
}
export function saveWord(word) {
  word.lastEdited = Date.now();
  storageUtils.saveWord(word);

  return (dispatch) => {
    dispatch(resetWord());
    dispatch(getWords());
    dispatch(getTags());
  };
}
export function deleteWord(word) {
  storageUtils.deleteWord(word);
  return (dispatch) => {
    dispatch(getTrash());
    dispatch(getWords());
  };
}
export function deleteWordPermenantly(word) {
  storageUtils.deleteWordPermenantly(word);
  return (dispatch) => {
    dispatch(getTrash());
  };
}
export function restoreWord(word) {
  storageUtils.restoreWord(word);
  return (dispatch) => {
    dispatch(getTrash());
    dispatch(getWords());
  };
}
function cleanWord(word) {
  let defaultWord = _.cloneDeep(newWordtemplate);
  let cleanedWord = Object.assign({}, defaultWord, word);
  if(cleanedWord.definitions.length === 0) {
    cleanedWord.definitions = defaultWord.definitions;
  }
  if (cleanedWord.definitions.length === 1) {
    cleanedWord.definitions.push(defaultWord.definitions[1]);
  }
  const cleanedDefinitions =  cleanedWord.definitions.map(def => {
    let d = Object.assign({}, defaultWord.definitions[0], def);

    d.otherLanguages = Object.assign({}, defaultWord.definitions[0].otherLanguages, d.otherLanguages);
    d.type = d.type.toUpperCase();
    d.synonyms = d.synonyms.map(e => e.trim().toLowerCase());
    return d;
  });
  cleanedWord.definitions = cleanedDefinitions;
  cleanedWord.wordText = cleanedWord.wordText.trim().toLowerCase();
  cleanedWord.tags = cleanedWord.tags.map(e => e.trim().toLowerCase());
  return cleanedWord;
}
function sortByWordText(a, b) {
  return a.wordText.localeCompare(b.wordText);
}
const initialState = fromJS({
  words: [],
  trash: [],
  word: _.cloneDeep(newWordtemplate),
  allTags: []
});

export default function wordState(state = initialState, action) {
  let nextState = state;
  switch (action.type) {
    case RECEIVE_WORDS: {
      nextState = state.set('words', fromJS(action.words.sort(sortByWordText)));
    }
      break;
    case RECEIVE_TRASH: {
      nextState = state.set('trash', fromJS(action.trash));
    }
      break;
    case RECEIVE_WORD: {
      nextState = state.set('word', fromJS(cleanWord(action.word)));
    }
      break;
    case RESET_WORD: {
      nextState = state.set('word', fromJS(_.cloneDeep(newWordtemplate)));
    }
      break;
    case RECEIVE_TAGS: {
      nextState = state.set('allTags', fromJS(action.allTags));
    }

      break;
    default:
  }
  return nextState;
}

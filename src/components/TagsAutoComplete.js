import React, {Component, PropTypes} from 'react';
import AutoComplete from 'material-ui/AutoComplete';
/**
 * `AutoComplete` search text can be implemented as a controlled value,
 * where `searchText` is handled by state in the parent component.
 * That value is reseted with the `onNewRequest` callback.
 */
export default class TagsAutoComplete extends Component {
  static propTypes= {
    hintText: PropTypes.string,
    allTags: PropTypes.array,
    onAddTag: PropTypes.func
  }
  static defaultProps={
    allTags: []
  }
  state = {
    searchText: '',
  };

  handleUpdateInput = (searchText) => {
    this.setState({
      searchText: searchText,
    });
  };

  handleNewRequest = (newTag) => {
    this.props.onAddTag(newTag);
    this.setState({
      searchText: '',
    });
  };

  render() {
    return (
        <AutoComplete
          hintText={this.props.hintText}
          searchText={this.state.searchText}
          onUpdateInput={this.handleUpdateInput}
          onNewRequest={this.handleNewRequest}
          dataSource={this.props.allTags}
          filter={(searchText, key) => (key.indexOf(searchText) !== -1)}
          openOnFocus={true}
        />
    );
  }
}

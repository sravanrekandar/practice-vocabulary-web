import React from 'react';
import {Link} from 'react-router';

const HomePage = () => {
  return (
    <div>

      <h1>Vocabulary Builder</h1>

      <p>
        <Link to="/all-words">All words</Link>
      </p>
    </div>
  );
};

export default HomePage;

import React, { PropTypes } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

export default function SelectFieldBox(props) {
  const {
    model: valueModel,
    title,
    items,
    ...rest
  } = props;
  const handleChange = function (e, idx, value) {
    valueModel.value = value;
  };
  return (
    <SelectField
      style={{ width: 150 }}
      value={valueModel.value}
      onChange={handleChange}
      title={title}
      {...rest}
    >
      {
        items.map((item, idx) => (
          <MenuItem
            key={`select-box-${title}-${idx}`}
            value={item}
            primaryText={item.toLowerCase()}
          />
        ))
      }

    </SelectField>
  );
}
SelectFieldBox.propTypes = {
  model: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(PropTypes.string)
};

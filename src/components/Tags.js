import React, {PropTypes} from 'react';
import Chip from 'material-ui/Chip';
import TagsAutoComplete from './TagsAutoComplete';
export default class Tags extends React.Component {
  static propTypes= {
    setId: PropTypes.string,
    label: PropTypes.string,
    hintText: PropTypes.string,
    tags: PropTypes.array,
    allTags: PropTypes.array,
    onRemoveTag: PropTypes.func,
    onAddTag: PropTypes.func
  }
  static defaultProps = {
    key: Date.now().toString(),
    tags : []
  }
  constructor(props) {
    super(props);
  }

  onRemoveTag = (tagName) => {
    this.props.onRemoveTag(tagName);
  };
  onAddTag = (tagName) => {
    this.props.onAddTag(tagName);
  };
  renderChip(name, idx) {
    return (
      <li key={`$tag-${name}-${idx}`}>
        <Chip
          onRequestDelete={() => this.onRemoveTag(name)}
          style={{margin: 4}}
        >
          {name}
        </Chip>
      </li>
    );
  }

  render() {
    const list = this.props.tags;
    return (
      <ul className="list-inline" style={{display: 'inline-block'}}>
        {list.map(this.renderChip, this)}
        <li>
          <TagsAutoComplete
            hintText={this.props.hintText}
            allTags={this.props.allTags}
            onAddTag={this.props.onAddTag}
          />
        </li>
      </ul>
    );
  }
}

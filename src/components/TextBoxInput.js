import React, { PropTypes } from 'react';
import TextField from 'material-ui/TextField';

export default function TextBoxInput(props) {
  const {
    model: valueModel,
    hintText,
    floatingLabelText,
    ...rest
  } = props;
  const handleChange = function (e) {
    valueModel.value = e.target.value;
  };
  return (
    <TextField
      hintText={hintText}
      floatingLabelText={floatingLabelText}
      onChange={handleChange}
      value={valueModel.value}
      {...rest}
    />
  );
}
TextBoxInput.propTypes = {
  model: PropTypes.object,
  hintText: PropTypes.string,
  floatingLabelText: PropTypes.string
};

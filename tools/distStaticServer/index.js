/* eslint-disable no-console */
const os = require('os');
const express = require('express');
const path = require('path');
const DEFAULT_PORT = 3333;
const app = express();
app.use(express.static(path.resolve(__dirname, '../../dist')));
app.set('port', (process.env.PORT || DEFAULT_PORT));
const PORT = app.get('port');
app.listen(app.get('port'), () => {
  console.log(`Practice-vocabulary-weblistening on http://${os.hostname()}:${PORT}`);
});
